package main

import (
	"flag"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"time"
)

const (
	file = "index.html"
)

var port string
var global *Category
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func main() {
	var p = flag.Int("port", 12500, "port to listen on")
	flag.Parse()
	port = fmt.Sprintf(":%d", *p)

	setup()

	fmt.Println("Starting server")
	serve_err := http.ListenAndServe(port, nil)
	if serve_err != nil {
		log.Fatalln("Error http.ListenAndServe: %v", serve_err)
	}
}

func setup() {
	db_chan := StartDBDefault()

	rand.Seed(time.Now().Unix())
	global = new(Category)
	global.Setup("Global", TGlobal, nil)
	global.GoListen()

	setupHandlers(db_chan)
}

func setupHandlers(db_chan chan DBCall) {
	// -/api/
	http.HandleFunc("/api/", func(w http.ResponseWriter, r *http.Request) {
		url, _ := getUrlAndParts(r, 0)
		log.Println("/api/ handler:", url)
		http.NotFound(w, r)
	})

	// -/api/timer/
	if apiTimerSetup(db_chan) {
		http.HandleFunc("/api/timer/", func(w http.ResponseWriter, r *http.Request) {
			apiTimerHandler(w, r, db_chan)
		})
	} else {
		log.Println("/api/timer/ failure")
	}

	// -/api/stopwatch/
	if apiStopWatchSetup(db_chan) {
		http.HandleFunc("/api/stopwatch/", func(w http.ResponseWriter, r *http.Request) {
			apiStopWatchHandler(w, r, db_chan)
		})
	} else {
		log.Println("/api/stopwatch/ failure")
	}

	// -/api/stopwatch/
	if apiLeaderBoardSetup(db_chan) {
		http.HandleFunc("/api/leaderboard/", func(w http.ResponseWriter, r *http.Request) {
			apiLeaderBoardHandler(w, r, db_chan)
		})
	} else {
		log.Println("/api/leaderboard/ failure")
	}

	// -/api/dice/
	if apiDiceSetup(db_chan) {
		http.HandleFunc("/api/dice/", func(w http.ResponseWriter, r *http.Request) {
			apiDiceHandler(w, r, db_chan)
		})
	} else {
		log.Println("/api/dice/ failure")
	}

	// -/chat/
	http.HandleFunc("/api/chat/", func(w http.ResponseWriter, r *http.Request) {
		websocketHandler(w, r, global.Messages)
	})
	// -/chat
	http.HandleFunc("/api/chat", func(w http.ResponseWriter, r *http.Request) {
		websocketHandler(w, r, global.Messages)
	})
}

func websocketHandler(w http.ResponseWriter, r *http.Request, con chan *Message) {
	url, _ := getUrlAndParts(r, 0)
	log.Println("/ websocket:", url)
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("websocketHandler setup error: ", err)
		return
	}

	input := make(chan string)
	output := make(chan string)
	user := new(User)
	user.Setup(input, output)
	running := true

	user.GoListen(con)

	// input
	go func() {
		defer func() {
			log.Printf("%s: input cleaning", user.Name)
			running = false
			close(user.Input)
		}()

		for running {
			msgType, msg, err := conn.ReadMessage()
			if !running || err != nil {
				return
			}

			if msgType != websocket.TextMessage {
				continue
			}

			user.Input <- string(msg)
		}
	}()

	// output
	go func() {
		defer func() {
			log.Printf("%s: output cleaning", user.Name)
			running = false
		}()

		for running {
			msg, ok := <-user.Output
			if !running || !ok {
				return
			}

			err = conn.WriteMessage(websocket.TextMessage, []byte(msg))
			if !running || err != nil {
				return
			}
		}
	}()
}

func msgTypeString(m int) string {
	switch m {
	case websocket.TextMessage:
		return "TextMessage"
	case websocket.BinaryMessage:
		return "BinaryMessage"
	case websocket.CloseMessage:
		return "CloseMessage"
	case websocket.PingMessage:
		return "PingMessage"
	case websocket.PongMessage:
		return "PongMessage"
	default:
		return fmt.Sprintf("unknown: %d", m)
	}
}

func getUrlAndParts(r *http.Request, drop int) (string, []string) {
	url := strings.Replace(r.URL.Path, "../", "", -1)
	if url[len(url)-1] != '/' {
		url += "/"
	}

	parts := strings.Split(url, "/")
	if len(parts[0]) == 0 {
		parts = parts[1:]
	}

	lindex := len(parts) - 1
	if len(parts[lindex]) == 0 {
		parts = parts[:lindex]
	}

	return url, parts[drop:]
}
