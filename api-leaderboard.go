package main

import (
	"fmt"
	"html"
	"log"
	"net/http"
	"strings"
)

/*
 * internal data
 */
type ApiLeaderBoardFunc func(http.ResponseWriter, []string, chan DBCall)

var leaderboard_data map[string]ApiLeaderBoardFunc

func apiLeaderBoardSetup(db_chan chan DBCall) bool {
	leaderboard_data = make(map[string]ApiLeaderBoardFunc)
	leaderboard_data["add"] = apiLeaderBoardAdd
	leaderboard_data["get"] = apiLeaderBoardGet
	if !DB_AddTable(db_chan, "leaderboard") {
		log.Println("apiLeaderBoardSetup DB_AddTable failed")
		return false
	}

	var fallblock LeaderBoardInfo
	fallblock.Init()
	return DB_Add(db_chan, "leaderboard", "fallblock", fallblock)
}

/*
 * handler and not found
 */
func apiLeaderBoardHandler(w http.ResponseWriter, r *http.Request, db_chan chan DBCall) {
	url, parts := getUrlAndParts(r, 2)
	log.Println("apiLeaderBoardHandler:", url)
	f, ok := leaderboard_data[parts[0]]
	if !ok {
		apiLeaderBoardNotFound(w, url)
		return
	}

	f(w, parts[1:], db_chan)
}

func apiLeaderBoardNotFound(w http.ResponseWriter, url string) {
	w.WriteHeader(http.StatusNotFound)
	fmt.Fprintf(w, "{\"ok\": %v, \"error\": \"%s: %s\", \"functions\": [\"%s\", \"%s\"]}",
		false,
		"Could not find function",
		html.EscapeString(url),
		html.EscapeString("/api/leaderboard/add/<key string>/<name string>/<score int>/"),
		html.EscapeString("/api/leaderboard/get/<key string>/"))
}

/*
 * response helpers
 */
func leaderboardResponseOK(w http.ResponseWriter, key string, leaderboard LeaderBoardInfo) {
	fmt.Fprintf(w, "{\"key\": \"%s\", \"ok\": %t, \"leaderboard\": %s}",
		key,
		true,
		leaderboard.ToJson())
}

func leaderboardResponseError(w http.ResponseWriter, key string, err string) {
	w.WriteHeader(http.StatusInternalServerError)
	fmt.Fprintf(w, "{\"key\": \"%s\", \"ok\": %t, \"error\": %s}",
		key,
		false,
		err)
}

func leaderboardResponseInvalidArgs(w http.ResponseWriter, expecting, got string) {
	w.WriteHeader(http.StatusBadRequest)
	fmt.Fprintf(w, "{\"ok\": %t, \"error\": %s, \"expecting\": %s, \"got\": %s}",
		false,
		"Invalid Arguments",
		expecting,
		got)
}

/*
 * APIs
 */
func apiLeaderBoardAdd(w http.ResponseWriter, parts []string, db_chan chan DBCall) {
	if len(parts) != 3 {
		leaderboardResponseInvalidArgs(w, "/api/leaderboard/add/<key string>/<name string>/<score int>/", strings.Join(parts, "/"))
		return
	}

	key := parts[0]
	name := parts[1]
	temp := parts[2]
	var score int
	if _, err := fmt.Sscanf(temp, "%d", &score); err != nil {
		leaderboardResponseError(w, key, fmt.Sprintf("Score reading error: %s", err))
		return
	}

	td, res := DB_Get(db_chan, "leaderboard", key)
	if !res {
		leaderboardResponseError(w, key, "LeaderBoard doesn't exist")
		return
	}

	leaderboard, ok := td.(LeaderBoardInfo)
	if !ok {
		leaderboardResponseError(w, key, "Invalid data")
		return
	}

	leaderboard.Add(name, score)

	if DB_Update(db_chan, "leaderboard", key, leaderboard) {
		leaderboardResponseOK(w, key, leaderboard)
	} else {
		leaderboardResponseError(w, key, "LeaderBoard already exists")
	}
}

func apiLeaderBoardGet(w http.ResponseWriter, parts []string, db_chan chan DBCall) {
	if len(parts) != 1 {
		leaderboardResponseInvalidArgs(w, "/api/leaderboard/get/<key string>/", strings.Join(parts, "/"))
		return
	}

	key := parts[0]
	td, res := DB_Get(db_chan, "leaderboard", key)
	if !res {
		leaderboardResponseError(w, key, "LeaderBoard doesn't exists")
		return
	}

	leaderboard, ok := td.(LeaderBoardInfo)
	if !ok {
		leaderboardResponseError(w, key, "Invalid data")
		return
	}

	leaderboardResponseOK(w, key, leaderboard)
}
