package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"
)

const letters = "abcdefghijklmnopqrstuvwxyz"
const connect_msg = "%s: Welcome to chat %s. use '/join category room' to join a room, or '/nick newname' to change your name"

const (
	TGlobal = iota
	TCategory
	TRoom
)

const (
	MConnect = iota
	MJoin
	MName
	MMessage
	MQuit
)

/*
 * Control structure for the different parts to send users
 */
type Message struct {
	Type    int
	Message string
	User    *User
	Chan    chan *Message
}

/*
 * User structure with very helpful name scrambling
 */
type User struct {
	Name      string
	Input     chan string
	Output    chan string
	Timestamp bool
	Messages  chan *Message
}

func (u *User) Setup(input chan string, output chan string) {
	u.RandName()
	u.Input = input
	u.Output = output
	u.Timestamp = true
	u.Messages = make(chan *Message, 100)
}

func (u *User) RandName() {
	n := ""
	for i := 0; i < 7; i++ {
		n = fmt.Sprintf("%s%c", n, letters[rand.Intn(len(letters))])
	}
	u.Name = string(n)
}

func (u *User) UserInput(msg string) (*Message, string) {
	if len(msg) == 0 {
		return nil, "No message"
	}

	var m *Message
	if msg[0] == '/' {
		parts := strings.Split(msg, " ")
		switch strings.ToLower(parts[0]) {
		case "/timestamp":
			u.Timestamp = !u.Timestamp
			return nil, ""

		case "/name":
			fallthrough
		case "/nick":
			if len(parts) < 2 {
				return nil, "Usage: /name newname"
			}

			m = new(Message)
			m.Type = MName
			m.User = u
			m.Message = parts[1]
			return m, ""

		case "/join":
			if len(parts) < 2 {
				return nil, "Usage: /join category [room]"
			}

			m = new(Message)
			m.Type = MJoin
			m.User = u
			m.Message = strings.Join(parts[1:], " ")
			return m, ""

		case "/quit":
			m = new(Message)
			m.Type = MQuit
			m.User = u
			return m, ""
		}
	}

	m = new(Message)
	m.Type = MMessage
	m.User = u
	m.Message = fmt.Sprintf("%s: %s", u.Name, msg)
	return m, ""
}

func (u *User) GoListen(con chan *Message) {
	go func() {
		running := true
		defer func() {
			log.Printf("%s: control cleaning", u.Name)
			running = false
			close(u.Output)
		}()

		m := new(Message)
		m.Type = MConnect
		m.User = u
		con <- m
		for running {
			select {
			case msg, ok := <-u.Input:
				if !ok {
					msg = "/quit"
				}

				m, es := u.UserInput(msg)
				if len(es) > 0 {
					u.Output <- es
					break
				}

				if m != nil {
					con <- m
					if m.Type == MQuit {
						return
					}
				}

			case m, ok := <-u.Messages:
				if !ok {
					return
				}
				log.Printf("%s: %d | %s", u.Name, m.Type, m.Message)

				if m.Chan != nil {
					con = m.Chan
				}

				if len(m.Message) != 0 {
					msg := m.Message
					if u.Timestamp {
						now := time.Now().Format(time.RFC822)
						msg = fmt.Sprintf("%s | %s", now, m.Message)
					}
					u.Output <- msg
				}
			}
		}
	}()
}

/*
 * Category (or Room)
 *  monitors for joins and nick changes
 *  Broadcasts chats
 */
type Category struct {
	Type       int
	Name       string
	Users      map[string]*User
	Parent     *Category
	Categories map[string]*Category
	Messages   chan *Message
	File       *os.File
}

func (c *Category) Setup(name string, t int, parent *Category) {
	c.Type = t
	c.Name = name
	c.Users = make(map[string]*User)

	switch t {
	case TGlobal:
		c.Categories = make(map[string]*Category)
	case TCategory:
		c.Parent = parent
		c.Categories = make(map[string]*Category)
	case TRoom:
		c.Parent = parent
	}

	c.Messages = make(chan *Message, 100)

	os.MkdirAll(c.Directory(), 0755)
	f, err := os.OpenFile(c.FileName(), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Printf("%s failed to open file: %s", c.Name, c.FileName())
		return
	}

	c.File = f
}

func (c *Category) GoListen() {
	go func() {
		for {
			m := <-c.Messages
			log.Printf("%s: %d | %v | %s", c.FullName(), m.Type, m.User == nil, m.Message)
			switch m.Type {
			case MConnect:
				c.MakeUserNameOk(m.User)
				c.Users[m.User.Name] = m.User
				m.User.Messages <- c.WelcomeMessage(m.User)
			// MConnect
			case MJoin:
				if len(m.Message) == 0 {
					break
				}

				parts := strings.Split(m.Message, " ")
				if len(parts) == 0 {
					break
				}

				u, exists := c.Users[m.User.Name]
				switch c.Type {
				case TGlobal:
					if parts[0] == c.Name {
						if exists && u == m.User {
							break
						} else if exists {
							c.MakeUserNameOk(m.User)
						}

						c.Users[m.User.Name] = m.User
						m.User.Messages <- c.WelcomeMessage(m.User)
					} else {
						if exists && u == m.User {
							delete(c.Users, m.User.Name)
						}

						t, ok := c.Categories[parts[0]]
						if !ok {
							t = new(Category)
							t.Setup(parts[0], TCategory, c)
							c.Categories[t.Name] = t
							t.GoListen()
						}

						t.Messages <- m
					}

				case TCategory:
					if parts[0] == c.Name && len(parts) == 1 {
						if exists && u == m.User {
							break
						} else if exists {
							c.MakeUserNameOk(m.User)
						}

						c.Users[m.User.Name] = m.User
						m.User.Messages <- c.WelcomeMessage(m.User)
					} else if parts[0] == c.Name && len(parts) > 1 {
						if exists && u == m.User {
							delete(c.Users, m.User.Name)
						}

						t, ok := c.Categories[parts[1]]
						if !ok {
							t = new(Category)
							t.Setup(parts[1], TRoom, c)
							c.Categories[t.Name] = t
							t.GoListen()
						}

						t.Messages <- m
					} else {
						c.Parent.Messages <- m
					}

				case TRoom:
					if len(parts) < 2 || parts[0] != c.Parent.Name || parts[1] != c.Name {
						if exists && u == m.User {
							delete(c.Users, m.User.Name)
						}
						c.Parent.Messages <- m
					} else {
						if exists && u == m.User {
							break
						} else if exists {
							c.MakeUserNameOk(m.User)
						}

						c.Users[m.User.Name] = m.User
						m.User.Messages <- c.WelcomeMessage(m.User)
					}
				}
			// MJoin
			case MName:
				newname := m.Message
				oldname := m.User.Name
				_, exists := c.Users[newname]
				if !exists {
					delete(c.Users, oldname)
					m.User.Name = newname
					c.Users[m.User.Name] = m.User
					m.User.Messages <- c.NameChangeMessage(newname, oldname)
				} else {
					m.User.Messages <- c.NameChangeFailedMessage()
				}
			// MName
			case MMessage:
				if len(m.Message) == 0 {
					break
				}

				c.Broadcast(m.Message, m.User != nil)
			// MMessage
			case MQuit:
				_, exists := c.Users[m.User.Name]
				if !exists {
					break
				}

				close(m.User.Messages)
				delete(c.Users, m.User.Name)
				// MQuit
			}
		}
	}()
}

func (c *Category) WelcomeMessage(u *User) *Message {
	m := new(Message)
	m.Type = MMessage
	m.Chan = c.Messages
	m.Message = fmt.Sprintf("%s: Welcome %s", c.FullName(), u.Name)
	return m
}

func (c *Category) NameChangeMessage(newname, oldname string) *Message {
	m := new(Message)
	m.Type = MMessage
	m.Message = fmt.Sprintf("%s: Name changed: %s -> %s", c.FullName(), oldname, newname)
	return m
}

func (c *Category) NameChangeFailedMessage() *Message {
	m := new(Message)
	m.Type = MMessage
	m.Message = fmt.Sprintf("%s: Name already in use.", c.FullName())
	return m
}

func (c *Category) Broadcast(msg string, from_user bool) {
	if c.File != nil && from_user {
		now := time.Now().Format(time.RFC822)
		c.File.WriteString(fmt.Sprintf("%s | %s\n", now, msg))
	}

	m := new(Message)
	m.Type = MMessage
	m.Message = msg
	for _, u := range c.Users {
		u.Messages <- m
	}

	if c.Parent != nil {
		m = new(Message)
		m.Type = MMessage
		m.Message = fmt.Sprintf("%s %s", c.Name, msg)
		c.Parent.Messages <- m
	}
}

func (c *Category) MakeUserNameOk(u *User) {
	_, exists := c.Users[u.Name]
	for exists {
		u.RandName()
		_, exists = c.Users[u.Name]
	}
}

func (c *Category) FullName() string {
	ret := c.Name
	for p := c.Parent; p != nil && p.Parent != nil; p = p.Parent {
		ret = fmt.Sprintf("%s %s", p.Name, ret)
	}

	return ret
}

func (c *Category) FileName() string {
	ret := fmt.Sprintf("%s.txt", c.Name)
	dir := c.Directory()
	if dir != "" {
		ret = fmt.Sprintf("%s/%s", dir, ret)
	}

	return ret
}

func (c *Category) Directory() string {
	ret := ""
	for p := c.Parent; p != nil && p.Parent != nil; p = p.Parent {
		ret = fmt.Sprintf("%s/%s", p.Name, ret)
	}

	return fmt.Sprintf("logs/%s", ret)
}
