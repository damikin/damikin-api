package main

import (
	"fmt"
	"strings"
	"time"
)

type LeaderBoardEntry struct {
	Time  time.Time
	Name  string
	Score int
}

func (lbe LeaderBoardEntry) String() string {
	return fmt.Sprintf("Name: %s, Score: %s, Time: %v", lbe.Name, lbe.Score, lbe.Time)
}

func (lbe LeaderBoardEntry) ToJson() string {
	return fmt.Sprintf("{\"name\": \"%s\",  \"score\": %d, \"time\": %d}", lbe.Name, lbe.Score, lbe.Time.Unix())
}

type LeaderBoardInfo struct {
	Scores []LeaderBoardEntry
}

func (lbi LeaderBoardInfo) String() string {
	ret := make([]string, len(lbi.Scores))
	for i := 0; i < len(lbi.Scores); i++ {
		ret[i] = lbi.Scores[i].String()
	}

	return strings.Join(ret, " | ")
}

func (lbi LeaderBoardInfo) ToJson() string {
	scores := make([]string, len(lbi.Scores))
	for i := 0; i < len(lbi.Scores); i++ {
		scores[i] = lbi.Scores[i].ToJson()
	}

	return fmt.Sprintf("{\"scores\": [%s]}", strings.Join(scores, ", "))
}

func (lbi *LeaderBoardInfo) Init() {
	lbi.Scores = make([]LeaderBoardEntry, 10)
	lbi.Scores[0] = LeaderBoardEntry{Name: "Frank", Score: 132, Time: time.Now()}
	lbi.Scores[1] = LeaderBoardEntry{Name: "James", Score: 119, Time: time.Now()}
	lbi.Scores[2] = LeaderBoardEntry{Name: "Albert", Score: 107, Time: time.Now()}
	lbi.Scores[3] = LeaderBoardEntry{Name: "AAA", Score: 80, Time: time.Now()}
	lbi.Scores[4] = LeaderBoardEntry{Name: "BBB", Score: 70, Time: time.Now()}
	lbi.Scores[5] = LeaderBoardEntry{Name: "CCC", Score: 60, Time: time.Now()}
	lbi.Scores[6] = LeaderBoardEntry{Name: "DDD", Score: 50, Time: time.Now()}
	lbi.Scores[7] = LeaderBoardEntry{Name: "EEE", Score: 40, Time: time.Now()}
	lbi.Scores[8] = LeaderBoardEntry{Name: "FFF", Score: 30, Time: time.Now()}
	lbi.Scores[9] = LeaderBoardEntry{Name: "GGG", Score: 20, Time: time.Now()}
}

func (lbi *LeaderBoardInfo) Add(name string, score int) bool {
	if lbi.AlreadyExists(name, score) {
		return false
	}

	for i := 0; i < len(lbi.Scores); i++ {
		if lbi.Scores[i].Score < score {
			for j := len(lbi.Scores) - 1; j > i; j-- {
				lbi.Scores[j] = lbi.Scores[j-1]
			}

			lbi.Scores[i] = LeaderBoardEntry{Name: name, Score: score, Time: time.Now()}
			return true
		}
	}

	return false
}

func (lbi LeaderBoardInfo) AlreadyExists(name string, score int) bool {
	for i := 0; i < len(lbi.Scores); i++ {
		val := lbi.Scores[i]
		if val.Name == name && val.Score == score {
			return true
		}
	}

	return false
}
