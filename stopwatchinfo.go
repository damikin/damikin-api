package main

import (
	"fmt"
	"time"
)

type StopWatchInfo struct {
	StartTime time.Time
	Duration  time.Duration
	Running   bool
	Laps      []int64
}

func (swi StopWatchInfo) ToString() string {
	return fmt.Sprintf("StartTime: %v | Duration: %v | Running: %t | Laps: %s", swi.StartTime, swi.Duration, swi.Running, swi.LapsJson())
}

func (swi StopWatchInfo) ToJson() string {
	return fmt.Sprintf("{\"starttime\": %d, \"duration\": %d, \"running\": %t, \"laps\": %s}",
		swi.StartTime.Unix(),
		int64(swi.Duration.Seconds()),
		swi.Running,
		swi.LapsJson())
}

func (swi StopWatchInfo) LapsJson() string {
	if swi.Laps == nil {
		return "[]"
	}

	ret := "["
	if len(swi.Laps) > 0 {
		ret += fmt.Sprintf("%d", swi.Laps[0])
	}

	for i := 1; i < len(swi.Laps); i++ {
		ret += fmt.Sprintf(", %d", swi.Laps[i])
	}
	ret += "]"

	return ret
}

func (swi *StopWatchInfo) Reset() {
	swi.Duration = time.Duration(0)
	swi.StartTime = time.Now()
	swi.Laps = nil
}

func (swi *StopWatchInfo) Start() {
	if !swi.Running {
		swi.StartTime = time.Now()
		swi.Running = true
	}
}

func (swi *StopWatchInfo) Stop() {
	if swi.Running {
		swi.Duration = swi.Duration + time.Now().Sub(swi.StartTime)
		swi.Running = false
	}
}

func (swi *StopWatchInfo) Lap() {
	if swi.Running {
		swi.Laps = append(swi.Laps, int64((time.Now().Sub(swi.StartTime) + swi.Duration).Seconds()))
	}
}
