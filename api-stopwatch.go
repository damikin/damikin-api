package main

import (
	"fmt"
	"html"
	"log"
	"net/http"
	"strings"
)

/*
 * internal data
 */
type ApiStopWatchFunc func(http.ResponseWriter, []string, chan DBCall)

var stopwatch_data map[string]ApiStopWatchFunc

func apiStopWatchSetup(db_chan chan DBCall) bool {
	stopwatch_data = make(map[string]ApiStopWatchFunc)
	stopwatch_data["start"] = apiStopWatchStart
	stopwatch_data["stop"] = apiStopWatchStop
	stopwatch_data["reset"] = apiStopWatchReset
	stopwatch_data["lap"] = apiStopWatchLap
	stopwatch_data["get"] = apiStopWatchGet
	stopwatch_data["add"] = apiStopWatchAdd
	return DB_AddTable(db_chan, "stopwatch")
}

/*
 * handler and not found
 */
func apiStopWatchHandler(w http.ResponseWriter, r *http.Request, db_chan chan DBCall) {
	url, parts := getUrlAndParts(r, 2)
	log.Println("apiStopWatchHandler:", url)
	f, ok := stopwatch_data[parts[0]]
	if !ok {
		apiStopWatchNotFound(w, url)
		return
	}

	f(w, parts[1:], db_chan)
}

func apiStopWatchNotFound(w http.ResponseWriter, url string) {
	w.WriteHeader(http.StatusNotFound)
	fmt.Fprintf(w, "{\"ok\": %v, \"error\": \"%s: %s\", \"functions\": [\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\"]}",
		false,
		"Could not find function",
		html.EscapeString(url),
		html.EscapeString("/api/stopwatch/start/<key string>/"),
		html.EscapeString("/api/stopwatch/stop/<key string>/"),
		html.EscapeString("/api/stopwatch/reset/<key string>/"),
		html.EscapeString("/api/stopwatch/lap/<key string>/"),
		html.EscapeString("/api/stopwatch/add/<key string>/"),
		html.EscapeString("/api/stopwatch/get/<key string>/"))
}

/*
 * response helpers
 */
func stopwatchResponseOK(w http.ResponseWriter, key string, stopwatch StopWatchInfo) {
	fmt.Fprintf(w, "{\"key\": \"%s\", \"ok\": %t, \"stopwatch\": %s}",
		key,
		true,
		stopwatch.ToJson())
}

func stopwatchResponseError(w http.ResponseWriter, key string, err string) {
	w.WriteHeader(http.StatusInternalServerError)
	fmt.Fprintf(w, "{\"key\": \"%s\", \"ok\": %t, \"error\": %s}",
		key,
		false,
		err)
}

func stopwatchResponseInvalidArgs(w http.ResponseWriter, expecting, got string) {
	w.WriteHeader(http.StatusBadRequest)
	fmt.Fprintf(w, "{\"ok\": %t, \"error\": %s, \"expecting\": %s, \"got\": %s}",
		false,
		"Invalid Arguments",
		expecting,
		got)
}

/*
 * APIs
 */
func apiStopWatchStart(w http.ResponseWriter, parts []string, db_chan chan DBCall) {
	if len(parts) != 1 {
		stopwatchResponseInvalidArgs(w, "/api/stopwatch/start/<key string>/", strings.Join(parts, "/"))
		return
	}

	key := parts[0]
	td, res := DB_Get(db_chan, "stopwatch", key)
	if !res {
		stopwatchResponseError(w, key, "StopWatch doesn't exists")
		return
	}

	stopwatch, ok := td.(StopWatchInfo)
	if !ok {
		stopwatchResponseError(w, key, "Invalid data")
		return
	}

	stopwatch.Start()
	if res = DB_Update(db_chan, "stopwatch", key, stopwatch); res {
		stopwatchResponseOK(w, key, stopwatch)
	} else {
		stopwatchResponseError(w, key, "Failed to update stopwatch")
	}
}

func apiStopWatchStop(w http.ResponseWriter, parts []string, db_chan chan DBCall) {
	if len(parts) != 1 {
		stopwatchResponseInvalidArgs(w, "/api/stopwatch/stop/<key string>/", strings.Join(parts, "/"))
		return
	}

	key := parts[0]
	td, res := DB_Get(db_chan, "stopwatch", key)
	if !res {
		stopwatchResponseError(w, key, "StopWatch doesn't exists")
		return
	}

	stopwatch, ok := td.(StopWatchInfo)
	if !ok {
		stopwatchResponseError(w, key, "Invalid data")
		return
	}

	stopwatch.Stop()
	if res = DB_Update(db_chan, "stopwatch", key, stopwatch); res {
		stopwatchResponseOK(w, key, stopwatch)
	} else {
		stopwatchResponseError(w, key, "Failed to update stopwatch")
	}
}

func apiStopWatchReset(w http.ResponseWriter, parts []string, db_chan chan DBCall) {
	if len(parts) != 1 {
		stopwatchResponseInvalidArgs(w, "/api/stopwatch/reset/<key string>/", strings.Join(parts, "/"))
		return
	}

	key := parts[0]
	td, res := DB_Get(db_chan, "stopwatch", key)
	if !res {
		stopwatchResponseError(w, key, "StopWatch doesn't exists")
		return
	}

	stopwatch, ok := td.(StopWatchInfo)
	if !ok {
		stopwatchResponseError(w, key, "Invalid data")
		return
	}

	stopwatch.Reset()
	if res = DB_Update(db_chan, "stopwatch", key, stopwatch); res {
		stopwatchResponseOK(w, key, stopwatch)
	} else {
		stopwatchResponseError(w, key, "Failed to update stopwatch")
	}
}

func apiStopWatchLap(w http.ResponseWriter, parts []string, db_chan chan DBCall) {
	if len(parts) != 1 {
		stopwatchResponseInvalidArgs(w, "/api/stopwatch/lap/<key string>/", strings.Join(parts, "/"))
		return
	}

	key := parts[0]
	td, res := DB_Get(db_chan, "stopwatch", key)
	if !res {
		stopwatchResponseError(w, key, "StopWatch doesn't exists")
		return
	}

	stopwatch, ok := td.(StopWatchInfo)
	if !ok {
		stopwatchResponseError(w, key, "Invalid data")
		return
	}

	stopwatch.Lap()
	if res = DB_Update(db_chan, "stopwatch", key, stopwatch); res {
		stopwatchResponseOK(w, key, stopwatch)
	} else {
		stopwatchResponseError(w, key, "Failed to update stopwatch")
	}
}

func apiStopWatchGet(w http.ResponseWriter, parts []string, db_chan chan DBCall) {
	if len(parts) != 1 {
		stopwatchResponseInvalidArgs(w, "/api/stopwatch/get/<key string>/", strings.Join(parts, "/"))
		return
	}

	key := parts[0]
	td, res := DB_Get(db_chan, "stopwatch", key)
	if !res {
		stopwatchResponseError(w, key, "StopWatch doesn't exists")
		return
	}

	stopwatch, ok := td.(StopWatchInfo)
	if !ok {
		stopwatchResponseError(w, key, "Invalid data")
		return
	}

	stopwatchResponseOK(w, key, stopwatch)
}

func apiStopWatchAdd(w http.ResponseWriter, parts []string, db_chan chan DBCall) {
	if len(parts) != 1 {
		stopwatchResponseInvalidArgs(w, "/api/stopwatch/add/<key string>/", strings.Join(parts, "/"))
		return
	}

	stopwatch := StopWatchInfo{}
	key := parts[0]
	if res := DB_Add(db_chan, "stopwatch", key, stopwatch); res {
		stopwatchResponseOK(w, key, stopwatch)
	} else {
		stopwatchResponseError(w, key, "StopWatch already exists")
	}
}
