package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"time"
)

func apiDiceSetup(db_chan chan DBCall) bool {
	rand.Seed(time.Now().UnixNano())
	return true
}

/*
 * handler and not found
 */
func apiDiceHandler(w http.ResponseWriter, r *http.Request, db_chan chan DBCall) {
	url, parts := getUrlAndParts(r, 2)
	log.Println("apiDiceHandler:", url)
	apiDiceGet(w, parts, db_chan)
}

func diceResponseOK(w http.ResponseWriter, result int) {
	fmt.Fprintf(w, "{\"ok\": %t, \"result\": %d}",
		true,
		result)
}

func diceResponseInvalidArgs(w http.ResponseWriter, expecting, got string) {
	w.WriteHeader(http.StatusBadRequest)
	fmt.Fprintf(w, "{\"ok\": %t, \"error\": %s, \"expecting\": %s, \"got\": %s}",
		false,
		"Invalid Arguments",
		expecting,
		got)
}

func apiDiceGet(w http.ResponseWriter, parts []string, db_chan chan DBCall) {
	if len(parts) != 2 {
		diceResponseInvalidArgs(w, "/api/dice/<num dice>/<num sides>/", strings.Join(parts, "/"))
		return
	}

	var num_dice, num_sides int
	fmt.Sscanf(parts[0], "%d", &num_dice)
	fmt.Sscanf(parts[1], "%d", &num_sides)
	if num_dice <= 0 {
		diceResponseInvalidArgs(w, "positive dice count", fmt.Sprintf("%d", num_dice))
		return
	}
	if num_sides <= 0 {
		diceResponseInvalidArgs(w, "positive side count", fmt.Sprintf("%d", num_sides))
		return
	}

	result := 0
	for i := 0; i < num_dice; i++ {
		result += rand.Intn(num_sides) + 1
	}

	diceResponseOK(w, result)
}
