package main

import (
	"fmt"
	"html"
	"log"
	"net/http"
	"strings"
)

/*
 * internal data
 */
type ApiTimerFunc func(http.ResponseWriter, []string, chan DBCall)

var timer_data map[string]ApiTimerFunc

func apiTimerSetup(db_chan chan DBCall) bool {
	timer_data = make(map[string]ApiTimerFunc)
	timer_data["start"] = apiTimerStart
	timer_data["stop"] = apiTimerStop
	timer_data["reset"] = apiTimerReset
	timer_data["get"] = apiTimerGet
	timer_data["add"] = apiTimerAdd
	timer_data["update"] = apiTimerUpdate
	return DB_AddTable(db_chan, "timer")
}

/*
 * handler and not found
 */
func apiTimerHandler(w http.ResponseWriter, r *http.Request, db_chan chan DBCall) {
	url, parts := getUrlAndParts(r, 2)
	log.Println("apiTimerHandler:", url)
	f, ok := timer_data[parts[0]]
	if !ok {
		apiTimerNotFound(w, url)
		return
	}

	f(w, parts[1:], db_chan)
}

func apiTimerNotFound(w http.ResponseWriter, url string) {
	w.WriteHeader(http.StatusNotFound)
	fmt.Fprintf(w, "{\"ok\": %v, \"error\": \"%s: %s\", \"functions\": [\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\"]}",
		false,
		"Could not find function",
		html.EscapeString(url),
		html.EscapeString("/api/timer/start/<key string>/"),
		html.EscapeString("/api/timer/stop/<key string>/"),
		html.EscapeString("/api/timer/reset/<key string>/"),
		html.EscapeString("/api/timer/get/<key string>/"),
		html.EscapeString("/api/timer/add/<key string>/<hours int>/<minutes int>/<seconds int>/"),
		html.EscapeString("/api/timer/update/<key string>/<hours int>/<minutes int>/<seconds int>/"))
}

/*
 * response helpers
 */
func timerResponseOK(w http.ResponseWriter, key string, timer TimerInfo) {
	fmt.Fprintf(w, "{\"key\": \"%s\", \"ok\": %t, \"timer\": %s}",
		key,
		true,
		timer.ToJson())
}

func timerResponseError(w http.ResponseWriter, key string, err string) {
	w.WriteHeader(http.StatusInternalServerError)
	fmt.Fprintf(w, "{\"key\": \"%s\", \"ok\": %t, \"error\": %s}",
		key,
		false,
		err)
}

func timerResponseInvalidArgs(w http.ResponseWriter, expecting, got string) {
	w.WriteHeader(http.StatusBadRequest)
	fmt.Fprintf(w, "{\"ok\": %t, \"error\": %s, \"expecting\": %s, \"got\": %s}",
		false,
		"Invalid Arguments",
		expecting,
		got)
}

/*
 * APIs
 */
func apiTimerStart(w http.ResponseWriter, parts []string, db_chan chan DBCall) {
	if len(parts) != 1 {
		timerResponseInvalidArgs(w, "/api/timer/start/<key string>/", strings.Join(parts, "/"))
		return
	}

	key := parts[0]
	td, res := DB_Get(db_chan, "timer", key)
	if !res {
		timerResponseError(w, key, "Timer doesn't exists")
		return
	}

	timer, ok := td.(TimerInfo)
	if !ok {
		timerResponseError(w, key, "Invalid data")
		return
	}

	timer.StartNow()
	if res = DB_Update(db_chan, "timer", key, timer); res {
		timerResponseOK(w, key, timer)
	} else {
		timerResponseError(w, key, "Failed to update timer")
	}
}
func apiTimerStop(w http.ResponseWriter, parts []string, db_chan chan DBCall) {
	if len(parts) != 1 {
		timerResponseInvalidArgs(w, "/api/timer/stop/<key string>/", strings.Join(parts, "/"))
		return
	}

	key := parts[0]
	td, res := DB_Get(db_chan, "timer", key)
	if !res {
		timerResponseError(w, key, "Timer doesn't exists")
		return
	}

	timer, ok := td.(TimerInfo)
	if !ok {
		timerResponseError(w, key, "Invalid data")
		return
	}

	timer.StopNow()
	if res = DB_Update(db_chan, "timer", key, timer); res {
		timerResponseOK(w, key, timer)
	} else {
		timerResponseError(w, key, "Failed to update timer")
	}
}

func apiTimerReset(w http.ResponseWriter, parts []string, db_chan chan DBCall) {
	if len(parts) != 1 {
		timerResponseInvalidArgs(w, "/api/timer/reset/<key string>/", strings.Join(parts, "/"))
		return
	}

	key := parts[0]
	td, res := DB_Get(db_chan, "timer", key)
	if !res {
		timerResponseError(w, key, "Timer doesn't exists")
		return
	}

	timer, ok := td.(TimerInfo)
	if !ok {
		timerResponseError(w, key, "Invalid data")
		return
	}

	timer.Reset()
	if res = DB_Update(db_chan, "timer", key, timer); res {
		timerResponseOK(w, key, timer)
	} else {
		timerResponseError(w, key, "Failed to update timer")
	}
}

func apiTimerGet(w http.ResponseWriter, parts []string, db_chan chan DBCall) {
	if len(parts) != 1 {
		timerResponseInvalidArgs(w, "/api/timer/get/<key string>/", strings.Join(parts, "/"))
		return
	}

	key := parts[0]
	td, res := DB_Get(db_chan, "timer", key)
	if !res {
		timerResponseError(w, key, "Timer doesn't exists")
		return
	}

	timer, ok := td.(TimerInfo)
	if !ok {
		timerResponseError(w, key, "Invalid data")
		return
	}

	timerResponseOK(w, key, timer)
}

func apiTimerAdd(w http.ResponseWriter, parts []string, db_chan chan DBCall) {
	if len(parts) != 4 {
		timerResponseInvalidArgs(w, "/api/timer/add/<key string>/<hours int>/<minutes int>/<seconds int>/", strings.Join(parts, "/"))
		return
	}

	var hours, minutes, seconds int
	fmt.Sscanf(parts[1], "%d", &hours)
	fmt.Sscanf(parts[2], "%d", &minutes)
	fmt.Sscanf(parts[3], "%d", &seconds)
	timer := TimerInfo{}
	timer.Set(hours, minutes, seconds)

	key := parts[0]
	if res := DB_Add(db_chan, "timer", key, timer); res {
		timerResponseOK(w, key, timer)
	} else {
		timerResponseError(w, key, "Timer already exists")
	}
}

func apiTimerUpdate(w http.ResponseWriter, parts []string, db_chan chan DBCall) {
	if len(parts) != 4 {
		timerResponseInvalidArgs(w, "/api/timer/update/<key string>/<hours int>/<minutes int>/<seconds int>/", strings.Join(parts, "/"))
		return
	}

	key := parts[0]
	td, ok := DB_Get(db_chan, "timer", key)
	if !ok {
		timerResponseError(w, key, "Timer doesn't exist")
		return
	}

	timer, ok := td.(TimerInfo)
	if !ok {
		timerResponseError(w, key, "Invalid data")
		return
	}

	var hours, minutes, seconds int
	fmt.Sscanf(parts[1], "%d", &hours)
	fmt.Sscanf(parts[2], "%d", &minutes)
	fmt.Sscanf(parts[3], "%d", &seconds)

	temp := timer.Duration
	timer.Set(hours, minutes, seconds)
	timer.Duration = temp

	if res := DB_Update(db_chan, "timer", key, timer); res {
		timerResponseOK(w, key, timer)
	} else {
		timerResponseError(w, key, "Timer dissapeared!")
	}
}
