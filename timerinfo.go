package main

import (
	"fmt"
	"log"
	"time"
)

type TimerInfo struct {
	StartTime time.Time
	Duration  time.Duration
	Total     time.Duration
	Running   bool
}

func (ti TimerInfo) ToString() string {
	return fmt.Sprintf("StartTime: %v | Duration: %v | Total: %v | Running: %t", ti.StartTime, ti.Duration, ti.Total, ti.Running)
}

func (ti TimerInfo) ToJson() string {
	return fmt.Sprintf("{\"starttime\": %d, \"duration\": %d, \"total\": %d, \"running\": %t, \"finish\": %d, \"remaining\": %d}",
		ti.StartTime.Unix(),
		int64(ti.Duration.Seconds()),
		int64(ti.Total.Seconds()),
		ti.Running,
		ti.Finish(),
		ti.Remaining())
}

func (ti TimerInfo) Finish() int64 {
	return ti.FinishTime().Unix()
}

func (ti TimerInfo) FinishTime() time.Time {
	now := ti.StartTime
	if !ti.Running {
		now = time.Now()
	}
	return now.Add(ti.Duration)
}

func (ti TimerInfo) Remaining() int64 {
	now := time.Now()
	end := ti.FinishTime()
	rem := end.Sub(now)
	return int64(rem.Seconds())
}

func (ti TimerInfo) IsFinished() bool {
	return time.Now().After(ti.FinishTime())
}

func (ti *TimerInfo) Set(hours, minutes, seconds int) {
	dstr := fmt.Sprintf("%dh%dm%ds", hours, minutes, seconds)
	temp, err := time.ParseDuration(dstr)
	if err != nil {
		log.Println("Error TimerInfo.Set: %v", err)
	}
	ti.Total = temp
	ti.Duration = ti.Total
}

func (ti *TimerInfo) Reset() {
	ti.Duration = ti.Total
	ti.StartTime = time.Now()
}

func (ti *TimerInfo) Start(unix int64) {
	if !ti.Running {
		ti.StartTime = time.Unix(unix, 0)
		ti.Running = true
	}
}

func (ti *TimerInfo) StartNow() {
	if !ti.Running {
		ti.Start(time.Now().Unix())
	}
}

func (ti *TimerInfo) Stop(unix int64) {
	if ti.Running {
		now := time.Unix(unix, 0)
		ti.Duration = ti.Duration - now.Sub(ti.StartTime)
		ti.Running = false
	}
}

func (ti *TimerInfo) StopNow() {
	if ti.Running {
		ti.Stop(time.Now().Unix())
	}
}
