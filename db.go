package main

import (
	"sync"
)

const (
	DBRowAdd = iota
	DBRowUpdate
	DBRowDelete
	DBRowGet
	DBRowExists
	DBTableAdd
	DBTableDelete
	DBTableExists
)

const (
	DBOK = iota
	DBRowAlreadyExists
	DBTableAlreadyExists
	DBRowDoesntExist
	DBTableDoesntExist
	DBInvalidKey
)

// Tables
type Table struct {
	rows map[string]interface{}
	mux  sync.RWMutex
}

func (t *Table) Setup() {
	t.rows = make(map[string]interface{})
}

func (t *Table) Add(key string, row interface{}) int {
	t.mux.Lock()
	defer t.mux.Unlock()

	_, ok := t.rows[key]
	if ok {
		return DBRowAlreadyExists
	}

	t.rows[key] = row
	return DBOK
}

func (t *Table) Update(key string, row interface{}) int {
	t.mux.Lock()
	defer t.mux.Unlock()

	_, ok := t.rows[key]
	if !ok {
		return DBRowDoesntExist
	}

	t.rows[key] = row
	return DBOK
}

func (t *Table) Delete(key string) int {
	t.mux.Lock()
	defer t.mux.Unlock()

	_, ok := t.rows[key]
	if !ok {
		return DBRowDoesntExist
	}

	delete(t.rows, key)
	return DBOK
}

func (t *Table) Get(key string) (interface{}, int) {
	t.mux.RLock()
	defer t.mux.RUnlock()

	row, ok := t.rows[key]
	if !ok {
		return nil, DBRowDoesntExist
	}

	return row, DBOK
}

func (t *Table) Exists(key string) bool {
	_, ok := t.Get(key)
	return ok == DBOK
}

// DB
type DB struct {
	tables map[string]*Table
	mux    sync.RWMutex
}

func (db *DB) Setup() {
	db.tables = make(map[string]*Table)
}

func (db *DB) Add(key string) int {
	db.mux.Lock()
	defer db.mux.Unlock()

	_, tok := db.tables[key]
	if tok {
		return DBTableAlreadyExists
	}

	table := new(Table)
	table.Setup()
	db.tables[key] = table
	return DBOK
}

func (db *DB) Delete(key string) int {
	db.mux.Lock()
	defer db.mux.Unlock()

	_, tok := db.tables[key]
	if !tok {
		return DBTableDoesntExist
	}

	delete(db.tables, key)
	return DBOK
}

func (db DB) Get(key string) (*Table, int) {
	db.mux.RLock()
	defer db.mux.RUnlock()

	table, tok := db.tables[key]
	if !tok {
		return nil, DBTableDoesntExist
	}

	return table, DBOK
}

// DB access
type DBCall struct {
	Command int
	Row     string
	Table   string
	Out     chan DBResult
	Data    interface{}
}

type DBResult struct {
	Result int
	Data   interface{}
}

func DB_Handler(in_chan chan DBCall, db *DB) {
	for call := range in_chan {
		result := DBResult{Result: DBOK}

		switch call.Command {
		case DBRowAdd:
			table, tok := db.Get(call.Table)
			if tok != DBOK {
				result.Result = tok
				break
			}

			rok := table.Add(call.Row, call.Data)
			result.Result = rok

		case DBRowUpdate:
			table, tok := db.Get(call.Table)
			if tok != DBOK {
				result.Result = tok
				break
			}

			rok := table.Update(call.Row, call.Data)
			result.Result = rok

		case DBRowDelete:
			table, tok := db.Get(call.Table)
			if tok != DBOK {
				result.Result = tok
				break
			}

			rok := table.Delete(call.Row)
			result.Result = rok

		case DBRowGet:
			table, tok := db.Get(call.Table)
			if tok != DBOK {
				result.Result = tok
				break
			}

			row, rok := table.Get(call.Row)
			result.Data = row
			result.Result = rok

		case DBRowExists:
			table, tok := db.Get(call.Table)
			if tok != DBOK {
				result.Result = tok
				break
			}

			exists := table.Exists(call.Row)
			if exists {
				result.Result = DBRowAlreadyExists
			} else {
				result.Result = DBRowDoesntExist
			}

		case DBTableAdd:
			tok := db.Add(call.Table)
			result.Result = tok

		case DBTableDelete:
			tok := db.Delete(call.Table)
			result.Result = tok
		}

		call.Out <- result
	}
}

// start the db
func StartDBDefault() chan DBCall {
	return StartDB(5)
}

func StartDB(db_cnt int) chan DBCall {
	in_chan := make(chan DBCall, 100)
	db := new(DB)
	db.Setup()
	for i := 0; i < db_cnt; i++ {
		go DB_Handler(in_chan, db)
	}
	return in_chan
}

// helpers
func DB_Add(db_chan chan DBCall, table, row string, data interface{}) bool {
	res_chan := make(chan DBResult)
	defer close(res_chan)
	db_chan <- DBCall{Command: DBRowAdd, Table: table, Row: row, Data: data, Out: res_chan}
	res := <-res_chan
	return res.Result == DBOK
}

func DB_Update(db_chan chan DBCall, table, row string, data interface{}) bool {
	res_chan := make(chan DBResult)
	defer close(res_chan)
	db_chan <- DBCall{Command: DBRowUpdate, Table: table, Row: row, Data: data, Out: res_chan}
	res := <-res_chan
	return res.Result == DBOK
}

func DB_Delete(db_chan chan DBCall, table, row string) bool {
	res_chan := make(chan DBResult)
	defer close(res_chan)
	db_chan <- DBCall{Command: DBRowDelete, Table: table, Row: row, Out: res_chan}
	res := <-res_chan
	return res.Result == DBOK
}

func DB_Get(db_chan chan DBCall, table, row string) (interface{}, bool) {
	res_chan := make(chan DBResult)
	defer close(res_chan)
	db_chan <- DBCall{Command: DBRowGet, Table: table, Row: row, Out: res_chan}
	res := <-res_chan
	return res.Data, res.Result == DBOK
}

func DB_Exists(db_chan chan DBCall, table, row string) bool {
	res_chan := make(chan DBResult)
	defer close(res_chan)
	db_chan <- DBCall{Command: DBRowExists, Table: table, Row: row, Out: res_chan}
	res := <-res_chan
	return res.Result == DBRowAlreadyExists
}

func DB_AddTable(db_chan chan DBCall, table string) bool {
	res_chan := make(chan DBResult)
	defer close(res_chan)
	db_chan <- DBCall{Command: DBTableAdd, Table: table, Out: res_chan}
	res := <-res_chan
	return res.Result == DBOK
}

func DB_DeleteTable(db_chan chan DBCall, table string) bool {
	res_chan := make(chan DBResult)
	defer close(res_chan)
	db_chan <- DBCall{Command: DBTableDelete, Table: table, Out: res_chan}
	res := <-res_chan
	return res.Result == DBOK
}

func DB_ExistsTable(db_chan chan DBCall, table string) bool {
	res_chan := make(chan DBResult)
	defer close(res_chan)
	db_chan <- DBCall{Command: DBTableExists, Table: table, Out: res_chan}
	res := <-res_chan
	return res.Result == DBTableAlreadyExists
}
