function grab_files(files, cb) {
	var worker = new Worker('/scripts/grabfiles_worker.js');
	worker.onmessage = function (e) {
		cb(e.data);
	}
	
	worker.postMessage(files);
}
