'use strict';

self.importScripts('generic.js');

let results = { };

function grab_file_worker(file) {
	doRequest('GET', file, function (e) {
		results[file] = e;
	}, true);
}

onmessage = function(e) {
	let data = e.data;
	for(let i = 0; i < data.length; i++) {
		grab_file_worker(data[i]);
	}
	
	postMessage(results);
}

