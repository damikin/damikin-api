'use strict';
let global_ws = {};
let history_length = 10;

/*
 * connect to websocket and setup everything
 */
function chat_connect(url, in_id, text_id) {
	if(!url || !in_id || !text_id) {
		console.log('chat_connect bad arguments: ', url, '|', in_id, '|', text_id);
		return;
	}
	
	// remove possibly saved input
	let chat = getElem(in_id);
	if(!chat) {
		console.log("chat_connect can't find: ", in_id);
		return;
	}
	chat.value = '';

	// websocket setup
	let absolute = url.startsWith('http') || url.startsWith('ws'),
		rf = (ignore, offset, string) => {
				if(offset != 0)
					return string;
				else
					return 'ws';
			};

	if(absolute) {
		url = url.replace('http', rf);
	} else {
		let t = document.URL.replace('http', rf);
		url = (new URL(url, t)).href;
	}

	let ws = new WebSocket(url, undefined);
	ws.addEventListener('message', function(event){
		let chat = getElem(text_id);
		if(!chat) {
			console.log("ws.onmessage can't find: ", text_id);
			return;
		}
		
		let hist = global_ws[in_id].history || [];
		hist.unshift(event.data);
		//hist = hist.splice(0, history_length);
		global_ws[in_id].history = hist;
		chat.innerHTML = hist.join("\n");
	});
	global_ws[in_id] = {ws: ws, history: []};
	
	// setup enter listener
	let elem = getElem(in_id) || document;
	elem.addEventListener('keydown', function(event) {
		if(!event.defaultPrevented && event.key === 'Enter') {
			chat_send(in_id);
			event.preventDefault();
		}
	});
}

/*
 * send a chat message
 */
function chat_send(in_id) {
	if(!global_ws[in_id]) {
		console.log('chat_send websocket not setup');
		return;
	}
	
	let ws = global_ws[in_id].ws;
	if(!ws) {
		console.log('chat_send websocket not setup');
		return;
	}
	
	let chat = getElem(in_id);
	if(!chat) {
		console.log("chat_send can't find: ", in_id);
		return;
	}
	
	let msg = chat.value;
	ws.send(msg);
	chat.value = '';
}

