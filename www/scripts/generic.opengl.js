'use strict';

function init_gl(canvas) {
	if(!canvas)
		return null;

	let gl = canvas.getContext('webgl')
		|| canvas.getContext('experimental-webgl');

	if(!gl)
		return null;

	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	return gl
}

function init_shaders(gl, shaders) {
	if(!shaders)
		shaders = [];

	let shader_program = gl.createProgram();
	
	for(let i = 0; i < shaders.length; i++) {
		let temp = make_shader(gl, shaders[i]);
		gl.attachShader(shader_program, temp);
	}

	gl.linkProgram(shader_program);
	if(!gl.getProgramParameter(shader_program, gl.LINK_STATUS)) {
		console.log(
			'an error occured while linking the shader program: ',
			gl.getProgramInfoLog(shader_program));
		return null;
	}

	gl.useProgram(shader_program);

	return shader_program;
}

function make_shader(gl, script, type) {
	if(!script)
		return null;

	if(!type) {
		switch(script.type) {
		case 'x-shader/x-fragment':
		case 'fragment':
			type = gl.FRAGMENT_SHADER;
			break;

		case 'x-shader/x-vertex':
		case 'vertex':
			type = gl.VERTEX_SHADER;
			break;

		default:
			// we don't know
			return null;
		}
	}

	let shader = gl.createShader(type);
	gl.shaderSource(shader, script.text);
	gl.compileShader(shader);

	if(!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		let type_text = 'UNKNOWN';
		switch(type) {
		case gl.FRAGMENT_SHADER:
			type_text = 'gl.FRAGMENT_SHADER';
			break;
		case gl.VERTEX_SHADER:
			type_text = 'gl.VERTEX_SHADER';
			break;
		}
		
		console.log(
			'an error occured while compiling the "' +
			type_text + '" shader:',
			gl.getShaderInfoLog(shader));

		gl.deleteShader(shader);
		return null;
	}

	return shader;
}

