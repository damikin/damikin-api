/*
 * timer updater
 */
function timer_run_update(key) {
	if(key != document.timer_key) {
		console.log('timer_run_update key != document.timer_key: ', key, ', ', document.timer_key);
		document.timer_running[key] = false;
		return;
	}

	var obj = document.timer_obj[key];
	if(obj.ok)
		timer_populate(obj);
	else
		timer_error(obj);

	if(document.timer_running[key] == false || obj.timer.running == false) {
		console.log('timer_run_update shouldn\'t be running: ', document.timer_running[key], ', ', obj.timer.running);
		document.timer_running[key] = false;
		return;
	}

	window.setTimeout(timer_run_update, 500, key);
}

function timer_run_periodic() {
	var key = document.timer_key;
	var url = '/api/timer/get/' + key + '/';
	doRequest('GET', url, function(resp) {
		if(!resp)
			return;

		var obj = JSON.parse(resp);
		document.timer_obj[key] = obj;
		if(!document.timer_running[key]) {
			timer_run(key, obj);
		}

		window.setTimeout(timer_run_periodic, 5000);
	});
}

function timer_run(key, obj) {
	if(!document.timer_running)
		document.timer_running = {};
	if(!document.timer_obj)
		document.timer_obj = {};

	if(!key) {
		console.log('timer_run needs a valid key: ', key);
		return;
	}

	if(document.timer_key != key)
		document.timer_running[document.timer_key] = false;

	document.timer_key = key;
	if(!obj || !obj.timer) {
		console.log('timer_run missing information: key: ', key, ', obj: ', obj);
		return;
	}

	document.timer_obj[key] = obj;
	document.timer_running[key] = obj.timer.running;
	timer_run_update(key);

	if(!document.timer_run_periodic_running) {
		document.timer_run_periodic_running = true;
		window.setTimeout(timer_run_periodic, 5000);
	}
}

/*
 * timer api calls
 */
function timer_get() {
	var key = getElem('timer_key').value;
	var url = '/api/timer/get/' + key + '/';
	doRequest('GET', url, function(resp) {
		if(!resp)
			return;

		var obj = JSON.parse(resp);
		timer_run(key, obj);
	});
}

/*
 * timer helpers
 */
function timer_populate(obj) {
	var timer = obj.timer;
	if(!timer) {
		console.log('timer_populate no timer: ', timer);
		return;
	}

	var finish = parseInt(timer.finish);
	var finish_str = '&nbsp;';
	var remaining = parseInt(timer.remaining);
	var total = parseInt(timer.total);
	if(timer.running) {
		finish_str = isNaN(finish) ? '&nbsp;' : (new Date(finish * 1000)).toLocaleString();
		remaining = timer.finish - Math.floor(Date.now() / 1000);
	}

	var timer_finish = getElem('timer_finish');
	if(timer_finish)
		timer_finish.innerHTML = finish_str;

	var timer_remaining = getElem('timer_remaining');
	if(timer_remaining)
		timer_remaining.innerHTML = timer_format_duration(remaining);

	var timer_total = getElem('timer_total');
	if(timer_total)
		timer_total.innerHTML = timer_format_duration(total);
}

function timer_format_duration(duration) {
	if(!duration && duration !== 0) {
		console.log('timer_format_duration no duration: ', duration);
		return '&nbsp;';
	}

	if(isNaN(duration)) {
		console.log('timer_format_duration duration isNaN: ', duration);
		return '&nbsp;';
	}

	var neg = '';
	if(duration < 0) {
		neg = '-';
		duration = Math.abs(duration);
	}

	var seconds = 0;
	var minutes = 0;
	var hours = 0;

	seconds = Math.floor(duration % 60);
	duration = Math.floor(duration / 60);
	minutes = Math.floor(duration % 60);
	duration = Math.floor(duration / 60);
	hours = Math.floor(duration % 60);

	if(seconds < 10) seconds = '0' + seconds;
	if(minutes < 10) minutes = '0' + minutes;
	if(hours < 10) hours = '0' + hours;

	return neg + hours + ':' + minutes + ':' + seconds;
}

function timer_error(obj) {
	var msg_area = getElem('message_area');
	if(!msg_area) {
		console.log('timer_error cannot report error: ', obj);
		return;
	}

	if(obj.ok) {
		msg_area.innerHTML = '';
		msg_area.style = 'display: none;';
		return;
	}

	msg_area.style = '';
	if(obj.error) {
		msg_area.innerHTML = 'Error: ' + obj.error;
	}

	if(obj.expected) {
		msg_area.innerHTML +=
			'<br />' +
			'Expected: ' + obj.expected;
	}

	if(obj.got) {
		msg_area.innerHTML +=
			'<br />' +
			'Got: ' + obj.got;
	}
}

