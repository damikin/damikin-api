'use strict';

function getElem(id) {
	if(!id)
		return null;

	let elem = document.getElementById(id);
	if(!elem)
		elem = null;

	return elem;
}

function doRequest(type, url, cb, sync) {
	let xhr = new XMLHttpRequest();
	xhr.open(type, url, !sync);

	let readystate = function() {
		if(xhr.readyState !== xhr.DONE)
			return;

		let resp = xhr.responseText;
		if(xhr.status !== 200) {
			console.log(
				url,
				' request failed: ',
				xhr.status,
				' | ',
				xhr.statusText,
				' | ',
				xhr.response);
			resp = null;
		}

		cb(resp);
	};

	if(!sync)
		xhr.onreadystatechange = readystate;

	xhr.send();

	if(!!sync)
		readystate();
}

function findThing(pred) {
    let func = ([x, ...xs], id) => {
        if(pred(x, id))
            return x;
        else if(xs)
            return func(xs, id)
        else
            return null;
    };

    return func;
}

