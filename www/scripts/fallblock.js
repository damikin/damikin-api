'use strict';

function fallblock_start() {
	var canvas = getElem('playarea');
	if(!canvas.getContext) {
		console.log('fallblock_start: getContext not supported: ', canvas);
		return;
	}

	document.fallblock_ctx = canvas.getContext('2d');
	fallblock_reset();
	document.addEventListener('keydown', fallblock_event_down, false);
	document.addEventListener('keyup', fallblock_event_up, false);
}

function fallblock_reset() {
	getElem('gameover').innerHTML = '&nbsp;';
	var ctx = document.fallblock_ctx;
	ctx.clearRect(0, 0, 450, 450);

	document.fallblock_obj = {
		x: 0,
		y: 400,
		xinc: 0,
		w: 50,
		h: 50,
		inc: 10,
		dec: -10,
	};
	document.fallblock_objs = [];
	document.fallblock_running = true;

	if(document.fallblock_interval)
		window.clearInterval(document.fallblock_interval);
	document.fallblock_score = 0;
	document.fallblock_interval = window.setInterval(fallblock_render, 1 / 60 * 1000);

	if(document.fallblock_blocks)
		window.clearInterval(document.fallblock_blocks);
	document.fallblock_blocks_cnt = 0;
	document.fallblock_blocks = window.setTimeout(fallblock_adder, 1000);

	fallblock_getleaderboard();
}

function fallblock_adder() {
	if(!document.fallblock_running)
		return;

	var obj = {
		x: Math.floor(Math.random() * 400),
		y: 0,
		yinc: 0,
		w: 50,
		h: 50,
		inc: 10,
	}

	document.fallblock_objs.push(obj);
	document.fallblock_blocks_cnt++;

	var time = 500 - document.fallblock_blocks_cnt * 10;
	if(time < 100)
		time = 100;

	document.fallblock_blocks = window.setTimeout(fallblock_adder, time);
}

function fallblock_render() {
	if(!document.fallblock_running)
		return;

	var obj = document.fallblock_obj;
	var objs = document.fallblock_objs;
	var ctx = document.fallblock_ctx;

	ctx.clearRect(obj.x, obj.y, obj.w, obj.h);
	obj.x += obj.xinc;
	if(obj.x > 400)
		obj.x = 400;
	else if (obj.x < 0)
		obj.x = 0;
	ctx.fillStyle = 'rgb(200, 0, 0)';
	ctx.fillRect(obj.x, obj.y, obj.w, obj.h);

	for(var i = 0; i < objs.length; i++) {
		ctx.clearRect(objs[i].x, objs[i].y, objs[i].w, objs[i].h);
		objs[i].y += objs[i].inc
		if(objs[i].y <= 400) {
			ctx.fillStyle = 'rgb(0, 0, 200)';
			ctx.fillRect(objs[i].x, objs[i].y, objs[i].w, objs[i].h);
		}

		if(obj.y < objs[i].y + objs[i].h) {
			if(objs[i].y <= 400 && !(obj.x > objs[i].x + objs[i].w || obj.x + obj.w < objs[i].x)) {
				console.log('fallblock_render collision! ', obj, ' | ', objs[i]);
				document.fallblock_running = false;
				getElem('gameover').innerHTML = 'Game Over';
				return;
			}
		}
	}

	while(objs.length > 0 && objs[0].y > 400) {
		document.fallblock_score++;
		objs.shift();
	}

	getElem('score').innerHTML = document.fallblock_score;
}

function fallblock_event_down(ev) {
	if(ev.defaultPrevented)
		return;

	const key = ev.key;
	var obj = document.fallblock_obj;

	switch(key) {
	case 'ArrowRight':
		obj.xinc = obj.inc;
		break;
	case 'ArrowLeft':
		obj.xinc = obj.dec;
		break;
	case ' ':
		if(!document.fallblock_running)
			fallblock_reset();
		break;
	default:
		return;
	}

	ev.preventDefault();
}

function fallblock_event_up(ev) {
	if(ev.defaultPrevented)
		return;

	const key = ev.key;
	var obj = document.fallblock_obj;

	switch(key) {
	case 'ArrowRight':
		if(obj.xinc == obj.inc)
			obj.xinc = 0;
		break;
	case 'ArrowLeft':
		if(obj.xinc == obj.dec)
			obj.xinc = 0;
		break;
	default:
		return;
	}

	ev.preventDefault();
}

function fallblock_getleaderboard() {
	doRequest("GET", "/api/leaderboard/get/fallblock/", function (resp) {
		var obj = JSON.parse(resp);
		var scores = obj.leaderboard.scores;
		var list = '';
		for(var i = 0; i < scores.length; i++) {
			list +=
				'<tr><td>' +
				scores[i].name +
				'</td><td>' +
				scores[i].score +
				'</td><td>' +
				(new Date(scores[i].time * 1000)).toLocaleString() +
				'</td></tr>';
		}
		getElem('leaderboard').innerHTML = list
	});
}

function fallblock_addleaderboard() {
	var name = getElem('leaderboard_name').value;
	if(!name) {
		alert('Enter a name!');
		return;
	}

	var score = document.fallblock_score;

	doRequest('POST', '/api/leaderboard/add/fallblock/' + name + '/' + score + '/', function (resp) {
		fallblock_getleaderboard();
	});
}

