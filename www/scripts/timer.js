/*
 * timer api calls
 */
function timer_add() {
	var key = getElem('timer_key').value;
	var hours = parseInt(getElem('timer_hours').value);
	var minutes = parseInt(getElem('timer_minutes').value);
	var seconds = parseInt(getElem('timer_seconds').value);
	if(isNaN(hours)) hours = 0;
	if(isNaN(minutes)) minutes = 0;
	if(isNaN(seconds)) seconds = 0;

	if(!key || (!hours && !minutes && !seconds)) {
		console.log('timer_add invalid input: key ', key, ', hours: ', hours, ', minutes: ', minutes, ', seconds: ', seconds);
		return;
	}

	var url = '/api/timer/add/' + key + '/' + hours + '/' + minutes + '/' + seconds + '/';
	doRequest('POST', url, function(resp) {
		if(!resp)
			return;

		var obj = JSON.parse(resp);
		timer_run(key, obj);
	});
}

function timer_reset() {
	var key = document.timer_key;
	if(!key) {
		console.log('timer_reset couldn\'t get key');
		return;
	}

	var url = '/api/timer/reset/' + key + '/';
	doRequest('GET', url, function(resp) {
		if(!resp)
			return;

		var obj = JSON.parse(resp);
		timer_run(key, obj);
	});
}

function timer_start() {
	var key = document.timer_key;
	if(!key) {
		console.log('timer_start couldn\'t get key');
		return;
	}

	var url = '/api/timer/start/' + key + '/';
	doRequest('GET', url, function(resp) {
		if(!resp)
			return;

		var obj = JSON.parse(resp);
		timer_run(key, obj);
	});
}

function timer_stop() {
	var key = document.timer_key;
	if(!key) {
		console.log('timer_stop couldn\'t get key');
		return;
	}

	var url = '/api/timer/stop/' + key + '/';
	doRequest('GET', url, function(resp) {
		if(!resp)
			return;

		var obj = JSON.parse(resp);
		timer_run(key, obj);
	});
}

function timer_update() {
	var key = getElem('timer_key').value;
	var hours = parseInt(getElem('timer_hours').value);
	var minutes = parseInt(getElem('timer_minutes').value);
	var seconds = parseInt(getElem('timer_seconds').value);
	if(isNaN(hours)) hours = 0;
	if(isNaN(minutes)) minutes = 0;
	if(isNaN(seconds)) seconds = 0;

	if(!key || (!hours && !minutes && !seconds)) {
		console.log('timer_update invalid input: key ', key, ', hours: ', hours, ', minutes: ', minutes, ', seconds: ', seconds);
		return;
	}

	var url = '/api/timer/update/' + key + '/' + hours + '/' + minutes + '/' + seconds + '/';
	doRequest('POST', url, function(resp) {
		if(!resp)
			return;

		var obj = JSON.parse(resp);
		timer_run(key, obj);
	});
}

