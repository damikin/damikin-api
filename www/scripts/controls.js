'use strict';

function controls_init() {
	document.controls = [];
	
	document.controls_keymapping = {
		ArrowLeft: 'look_left',
		ArrowRight: 'look_right',
		ArrowUp: 'forward',
		ArrowDown: 'backward',
		'a': 'left',
		'd': 'right',
		'w': 'forward',
		's': 'backward',
		Shift: 'up',
		Control: 'down',
		Escape: 'quit',
		Enter: 'reset',
		' ': 'pause',
	};
	
	document.addEventListener('keydown', controls_keydown);
	document.addEventListener('keyup', controls_keyup);
}

function controls_get() {
	let ret = document.controls.splice(0, 1)[0];
	if(!ret)
		ret = null;

	return ret;
}

function controls_clear() {
	document.controls = [];
}

function controls_add(key, down) {
	if(!key)
		return;
	if(key.length == 1)
		key = key.toLowerCase();

	let name = document.controls_keymapping[key];
	if(!name) {
		console.log('key not mapped: ', key);
		return;
	}

	document.controls.push({name: name, pressed: down});
}

function controls_keydown(event) {
	const key_name = event.key;
	
	controls_add(key_name, true);
}

function controls_keyup(event) {
	const key_name = event.key;
	
	controls_add(key_name, false);
}

