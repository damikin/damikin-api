'use strict';

function game_init() {
	let shader_files = [
		'/shaders/fragment.shader',
		'/shaders/vertex.shader'
	];
	
	grab_files(shader_files, function (shader_dict) {
		let shaders = [];
		for(var i = 0; i < shader_files.length; i++) {
			let shader_file = shader_files[i];
			let islash = shader_file.lastIndexOf('/');
			let idot = shader_file.lastIndexOf('.');
			let shader_type = shader_file.slice(islash + 1, idot);
			let shader_text = shader_dict[shader_file];
			shaders.push({type: shader_type, text: shader_text});
		}
		
		let canvas = getElem('playarea');
		let gl = init_gl(canvas);
		if(!gl) {
			alert('failed to init opengl');
			return;
		}
		let shader_program = init_shaders(gl, shaders);
		if(!shader_program) {
			alert('failed to get shader program');
			return;
		}
		
		let gl_obj = {
			canvas: canvas,
			gl: gl,
			shader_program: shader_program,
		};
		
		controls_init();
		game_start(gl_obj);
	});
}

function make_buffer(gl, verts) {
	let buf = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, buf);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(verts), gl.STATIC_DRAW);
	return buf;
}

function game_start(gl_obj) {
	let vpa = gl_obj.gl.getAttribLocation(gl_obj.shader_program, 'aVertexPosition');
	gl_obj.gl.enableVertexAttribArray(vpa);
	let verts1 = [
		1.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		1.0, 0.0, 0.0,
		0.0, 0.0, 0.0
	];
	let verts2 = [
		0.0, 0.0, 0.0,
		-1.0, 0.0, 0.0,
		0.0, -1.0, 0.0,
		-1.0, -1.0, 0.0
	];
	let bufs = [];
	bufs.push(make_buffer(gl_obj.gl, verts1));
	bufs.push(make_buffer(gl_obj.gl, verts2));
	
	let game_state = {
		running: true,
		reset: false,
		pause: false,
		x: 0,
		y: 0,
		z: 0,
	};
	
	let controls = {
		right: false,
		left: false,
		forward: false,
		backward: false,
		look_left: false,
		look_right: false,
		up: false,
		down: false,
		quit: false,
		pause: false,
		reset: false,
	};
	
	let game_loop = setInterval(function () {
		controls = controls_game(controls);
		if(controls.quit)
			game_state.running = false;
		if(controls.reset)
			game_state.reset = true;
		if(controls.pause) {
			game_state.pause = !game_state.pause;
			controls.pause = false;
		}
			
		if(!game_state.running) {
			if(game_state.reset) {
				clearInterval(game_loop);
				setTimeout(game_init, 1000 / 30);
			}
			return;
		}
		
		if(game_state.pause)
			return;
		
		if(controls.right)
			game_state.x -= 0.1;
		if(controls.left)
			game_state.x += 0.1;
		if(controls.forward)
			game_state.z += 0.1;
		if(controls.backward)
			game_state.z -= 0.1;
		if(controls.up)
			game_state.y -= 0.1;
		if(controls.down)
			game_state.y += 0.1;

		let mv_vec = $V([game_state.x, game_state.y, game_state.z]);
		let mv_mat = Matrix.Translation(mv_vec);
		draw(gl_obj, bufs, vpa, mv_mat);
	}, 1000 / 60, document.game_state);
	console.log('starting ', game_loop);
}

function controls_game(controls) {
	while(true) {
		let control = controls_get();
		if(!control)
			break;

		let name = control.name;
		let pressed = control.pressed;
		
		switch(name) {
		case 'left':
			controls.left = pressed;
			break;
		case 'right':
			controls.right = pressed;
			break;
		case 'forward':
			controls.forward = pressed;
			break;
		case 'backward':
			controls.backward = pressed;
			break;
		case 'look_left':
			controls.look_left = pressed;
			break;
		case 'look_right':
			controls.look_right = pressed;
			break;
		case 'up':
			controls.up = pressed;
			break;
		case 'down':
			controls.down = pressed;
			break;
		case 'quit':
			if(!pressed)
				controls.quit = true;
			break;
		case 'reset':
			if(!pressed) {
				controls.reset = true;
				controls.quit = true;
			}
			break;
		case 'pause':
			if(!pressed)
				controls.pause = true;
		}
	}
	
	return controls;
}

function draw(gl_obj, bufs, vpa, mv_mat) {
	let gl = gl_obj.gl;
	let width = gl_obj.canvas.width;
	let height = gl_obj.canvas.height;
	let shader_program = gl_obj.shader_program;
	
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	let per = makePerspective(45, width / height, 0.1, 100.0);
	mv_mat = mv_mat.x(Matrix.Translation($V([-0.0, 0.0, -6.0])).ensure4x4());
	let pUniform = gl.getUniformLocation(shader_program, 'uPMatrix');
	let mvUniform = gl.getUniformLocation(shader_program, 'uMVMatrix');
	let parr = new Float32Array(per.flatten());
	let marr = new Float32Array(mv_mat.flatten());

	for(let i = 0; i < bufs.length; i++) {
		let buf = bufs[i];
		gl.bindBuffer(gl.ARRAY_BUFFER, buf);
		gl.vertexAttribPointer(vpa, 3, gl.FLOAT, false, 0, 0);
		gl.uniformMatrix4fv(pUniform, false, parr);
		gl.uniformMatrix4fv(mvUniform, false, marr);
		gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
	}
}

