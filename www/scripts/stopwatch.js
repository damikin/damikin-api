/*
 * stopwatch api calls
 */
function stopwatch_add() {
	var key = getElem('stopwatch_key').value;
	if(!key) {
		console.log('stopwatch_add invalid: key ', key);
		return;
	}

	var url = '/api/stopwatch/add/' + key + '/';
	doRequest('POST', url, function(resp) {
		if(!resp)
			return;

		var obj = JSON.parse(resp);
		stopwatch_run(key, obj);
	});
}

function stopwatch_reset() {
	var key = document.stopwatch_key;
	if(!key) {
		console.log('stopwatch_reset couldn\'t get key');
		return;
	}

	var url = '/api/stopwatch/reset/' + key + '/';
	doRequest('GET', url, function(resp) {
		if(!resp)
			return;

		var obj = JSON.parse(resp);
		stopwatch_run(key, obj);
	});
}

function stopwatch_start() {
	var key = document.stopwatch_key;
	if(!key) {
		console.log('stopwatch_start couldn\'t get key');
		return;
	}

	var url = '/api/stopwatch/start/' + key + '/';
	doRequest('GET', url, function(resp) {
		if(!resp)
			return;

		var obj = JSON.parse(resp);
		stopwatch_run(key, obj);
	});
}

function stopwatch_stop() {
	var key = document.stopwatch_key;
	if(!key) {
		console.log('stopwatch_stop couldn\'t get key');
		return;
	}

	var url = '/api/stopwatch/stop/' + key + '/';
	doRequest('GET', url, function(resp) {
		if(!resp)
			return;

		var obj = JSON.parse(resp);
		stopwatch_run(key, obj);
	});
}

function stopwatch_lap() {
	var key = document.stopwatch_key;
	if(!key) {
		console.log('stopwatch_stop couldn\'t get key');
		return;
	}

	var url = '/api/stopwatch/lap/' + key + '/';
	doRequest('GET', url, function(resp) {
		if(!resp)
			return;

		var obj = JSON.parse(resp);
		stopwatch_run(key, obj);
	});
}

