/*
 * stopwatch updater
 */
function stopwatch_run_update(key) {
	if(key != document.stopwatch_key) {
		console.log('stopwatch_run_update key != document.stopwatch_key: ', key, ', ', document.stopwatch_key);
		document.stopwatch_running[key] = false;
		return;
	}

	var obj = document.stopwatch_obj[key];
	if(obj.ok)
		stopwatch_populate(obj);
	else
		stopwatch_error(obj);

	if(document.stopwatch_running[key] == false || obj.stopwatch.running == false) {
		console.log('stopwatch_run_update shouldn\'t be running: ', document.stopwatch_running[key], ', ', obj.stopwatch.running);
		document.stopwatch_running[key] = false;
		return;
	}

	window.setTimeout(stopwatch_run_update, 500, key);
}

function stopwatch_run_periodic() {
	var key = document.stopwatch_key;
	var url = '/api/stopwatch/get/' + key + '/';
	doRequest('GET', url, function(resp) {
		if(!resp)
			return;

		var obj = JSON.parse(resp);
		document.stopwatch_obj[key] = obj;
		if(!document.stopwatch_running[key]) {
			stopwatch_run(key, obj);
		}

		window.setTimeout(stopwatch_run_periodic, 5000);
	});
}

function stopwatch_run(key, obj) {
	if(!document.stopwatch_running)
		document.stopwatch_running = {};
	if(!document.stopwatch_obj)
		document.stopwatch_obj = {};

	if(!key) {
		console.log('stopwatch_run needs a valid key: ', key);
		return;
	}

	if(document.stopwatch_key != key)
		document.stopwatch_running[document.stopwatch_key] = false;

	document.stopwatch_key = key;
	if(!obj || !obj.stopwatch) {
		console.log('stopwatch_run missing information: key: ', key, ', obj: ', obj);
		return;
	}

	document.stopwatch_obj[key] = obj;
	document.stopwatch_running[key] = obj.stopwatch.running;
	stopwatch_run_update(key);

	if(!document.stopwatch_run_periodic_running) {
		document.stopwatch_run_periodic_running = true;
		window.setTimeout(stopwatch_run_periodic, 5000);
	}
}

/*
 * stopwatch api calls
 */
function stopwatch_get() {
	var key = getElem('stopwatch_key').value;
	var url = '/api/stopwatch/get/' + key + '/';
	doRequest('GET', url, function(resp) {
		if(!resp)
			return;

		var obj = JSON.parse(resp);
		stopwatch_run(key, obj);
	});
}

/*
 * stopwatch helpers
 */
function stopwatch_populate(obj) {
	var stopwatch = obj.stopwatch;
	if(!stopwatch) {
		console.log('stopwatch_populate no stopwatch: ', stopwatch);
		return;
	}

	var duration = parseInt(stopwatch.duration);
	var start = new Date(stopwatch.starttime * 1000);
	var now = Date.now();
	var stopwatch_duration = getElem('stopwatch_duration');
	if(stopwatch.running)
		stopwatch_duration.innerHTML = stopwatch_format_duration(duration + (now - start) / 1000);
	else
		stopwatch_duration.innerHTML = stopwatch_format_duration(duration);

	var laps = '<ul>';
	for(var i = 0; i < stopwatch.laps.length; i++)
		laps += '<li>' + stopwatch_format_duration(stopwatch.laps[i]) + '</li>';
	laps += '</ul>';

	var stopwatch_laps = getElem('stopwatch_laps');
	if(stopwatch_laps)
		stopwatch_laps.innerHTML = laps;
}

function stopwatch_format_duration(duration) {
	if(!duration && duration !== 0) {
		console.log('stopwatch_format_duration no duration: ', duration);
		return '&nbsp;';
	}

	if(isNaN(duration)) {
		console.log('stopwatch_format_duration duration isNaN: ', duration);
		return '&nbsp;';
	}

	var neg = '';
	if(duration < 0) {
		neg = '-';
		duration = Math.abs(duration);
	}

	var seconds = 0;
	var minutes = 0;
	var hours = 0;

	seconds = Math.floor(duration % 60);
	duration = Math.floor(duration / 60);
	minutes = Math.floor(duration % 60);
	duration = Math.floor(duration / 60);
	hours = Math.floor(duration % 60);

	if(seconds < 10) seconds = '0' + seconds;
	if(minutes < 10) minutes = '0' + minutes;
	if(hours < 10) hours = '0' + hours;

	return neg + hours + ':' + minutes + ':' + seconds;
}

function stopwatch_error(obj) {
	var msg_area = getElem('message_area');
	if(!msg_area) {
		console.log('stopwatch_error cannot report error: ', obj);
		return;
	}

	if(obj.ok) {
		msg_area.innerHTML = '';
		msg_area.style = 'display: none;';
		return;
	}

	msg_area.style = '';
	if(obj.error) {
		msg_area.innerHTML = 'Error: ' + obj.error;
	}

	if(obj.expected) {
		msg_area.innerHTML +=
			'<br />' +
			'Expected: ' + obj.expected;
	}

	if(obj.got) {
		msg_area.innerHTML +=
			'<br />' +
			'Got: ' + obj.got;
	}
}

